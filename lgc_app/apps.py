from django.apps import AppConfig


class LgcAppConfig(AppConfig):
    name = 'lgc_app'
