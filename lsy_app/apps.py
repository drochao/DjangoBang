from django.apps import AppConfig


class LsyAppConfig(AppConfig):
    name = 'lsy_app'
