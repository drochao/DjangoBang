
from django.conf.urls import url

from . import views # 导入同文件夹的模块

urlpatterns = [
    url(r'^$', views.go),
    url(r'^go/', views.go),
    url(r'^go2/', views.go2),
]
