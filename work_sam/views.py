from django.http import HttpResponse
from django.shortcuts import render
import time

# Create your views here.


def go(request):
    return HttpResponse("<h1><a href='/sam/go/'>页面1</a>&nbsp;&nbsp;<a href='/sam/go2/'>页面2</a><br><br>git，我们出发，这是第一个页面 --Sam</h1><br>")


def go2(request):
    now_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    print(type(now_time))
    return HttpResponse(f"<h1><a href='/sam/go/'>页面1</a>&nbsp;&nbsp;<a href='/sam/go2/'>页面2</a><br><br>git，我们出发, 这是第一个页面 --Sam <br> <br> 现在时刻：{now_time}</h1>")