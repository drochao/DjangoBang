from django.apps import AppConfig


class WorkSamConfig(AppConfig):
    name = 'work_sam'
